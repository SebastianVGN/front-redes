import axios from 'axios';

export async function registerPerson(person){
    let url = process.env.VUE_APP_API_URL + '/backRedes/insertarUsuarios';
    console.log(url);
    return axios.post(url, JSON.parse(JSON.stringify(person)));
}

export async function getPersons(){
    let url = process.env.VUE_APP_API_URL + '/backRedes/listarUsuarios';
    return axios.get(url);
}
import Vue from 'vue'
import Vuex from 'vuex'
import * as PersonService from '@/services/personService.js';

Vue.use(Vuex)

export default new Vuex.Store({
	state: {
		persons: null,
		names: null,
		lastnames: null,
	},
	mutations: {
		setPersons(state,persons){
			state.persons = persons;
		},
	},
	actions: {
		async getPersonsDA(context){
			PersonService.getPersons()
				.then(result => {
					const API_RESULT = result;
					console.log(API_RESULT.data);
					context.commit('setPersons', API_RESULT.data);
				})
				.catch(result => {
					const API_RESULT = result;
					console.log(API_RESULT);
				})
		},
		
	}
})

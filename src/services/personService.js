import * as PersonDA from '@/dataAccess/registerPerson.js';

export async function getPersons() {
    return PersonDA.getPersons();
}